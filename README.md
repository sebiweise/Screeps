[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sebiweise_Screeps&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sebiweise_Screeps)

# Screeps

I'm publishing the code I use for my Screeps colonies here. I didn't invent and develop everything myself. Many parts of my code have been copied, modified and adapted to my code.

Feel free to use this code

Screeps: https://screeps.com/

## Credits:
<a href="https://www.buymeacoffee.com/sebiweise" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-blue.png" alt="Buy Us A Coffee" height="51px" width="217px" /></a>
